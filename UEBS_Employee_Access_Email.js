/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope Public
 */

define(['N/record', 'N/email', 'N/runtime', 'N/search'], function(record, email, runtime, search) {
    var defaultPassword = 'GEODynamics2019';
    function sendEmail(obj) {
        try {
            email.send({
                author: runtime.getCurrentUser()['id'],
                recipients: obj['recipient'],
                replyTo: runtime.getCurrentUser()['email'],
                subject: obj['subject'],
                body: obj['body'],
                relatedRecords: {
                    entityId: obj['employee']
                }
            });
        } catch (e) {
            log.error(e.name, e.message);
        }
    }

    function beforeSubmit(context) {
        if (runtime.executionContext !== runtime.ContextType.USER_INTERFACE) {return;}
        var cr = context.newRecord;
        var firstname = cr.getValue('firstname');
        var email = cr.getValue('email');
        // New User Email
        if (cr.getValue('custentity_send_new_access_email') === true) {
            cr.setValue('custentity_send_new_access_email', false);
            sendEmail({
                employee: cr.id,
                recipient: email,
                subject: 'GEODynamics, Inc. - NetSuite User Access',
                body: 'Hi ' + firstname + ', welcome to GEODynamics! Your user access to NetSuite has been approved.' +
                    '\n\nLogin URL: https://system.netsuite.com' +
                    '\nEmail Address: ' + email +
                    '\nDefault Password: ' + defaultPassword +
                    '\n\nYou will be required to change the default password when you log in. If you run into any issues, please contact joseph.kim@perf.com'
            });
            var accessChangeRequest = cr.getValue('custentity_access_change_request_id');
            var giveAccess = cr.getValue('giveaccess');
            log.debug('accessChangeRequest', accessChangeRequest);
            log.debug('giveAccess', giveAccess);
            if (accessChangeRequest && giveAccess === true) {
                record.submitFields({
                    type: 'customrecord_access_change_request',
                    id: accessChangeRequest,
                    values: {
                        custrecord_acr_status: 'Approved, Access Granted'
                    },
                    options: {
                        enableSourcing: false,
                        ignoreMandatoryFields : true
                    }
                });
            }
        }
        // Password Reset Email
        if (cr.getValue('custentity_send_password_reset_email') === true) {
            cr.setValue('custentity_send_password_reset_email', false);
            sendEmail({
                employee: cr.id,
                recipient: email,
                subject: 'GEODynamics, Inc. - NetSuite Password Reset',
                body: 'Hi ' + firstname + ', your NetSuite password has been reset to the default password.' +
                    '\n\nLogin URL: https://system.netsuite.com' +
                    '\nEmail Address: ' + email +
                    '\nDefault Password: ' + defaultPassword +
                    '\n\nYou will be required to change the default password when you log in. If you run into any issues, please contact joseph.kim@perf.com'
            });
        }
    }
    return {
        beforeSubmit: beforeSubmit
    };

});