define(['N/ui/serverWidget', 'N/search', 'N/runtime'], function (ui, search, runtime) {
    /**
     *@NApiVersion 2.x
     *@NScriptType Suitelet
     *@NModuleScope Public
     */

    var exports = {};
    function onRequest(context) {
        var assemblyItems = getAssemblyItems();
        var plannedSearchCost = getPlannedStandardCost(assemblyItems);
        context.response.writePage({pageObject: renderList()});
    }

    function renderList() {
        return ui.createForm({title: 'GEODynamics Employee Center', hideNavBar: false});
    }

    function getAssemblyItems() {
        log.debug('getAssemblyItems', '>>> START <<<');
        var searchObj = search.create({
            type: 'assemblyitem',
            filters: [
                ['isinactive','is','F'],'AND',
                ['inventorylocation','anyof','10'],'AND',
                ['memberitem.internalidnumber','isnotempty',''],'AND',
                ['type','anyof','Assembly'],'AND',
                ['memberitem.isinactive','is','F']
            ],
            columns: [
                search.createColumn({name: 'internalid', sort: search.Sort.ASC}),
                'custitem3',
                'class',
                'itemid',
                'salesdescription',
                'costcategory',
                'memberitem',
                'memberitemsource',
                'memberquantity',
                search.createColumn({name: 'type', join: 'memberItem'}),
                search.createColumn({name: 'itemid', join: 'memberItem'}),
                search.createColumn({name: 'internalid', join: 'memberItem', sort: search.Sort.ASC}),
                search.createColumn({name: 'salesdescription', join: 'memberItem'}),
                search.createColumn({name: 'class', join: 'memberItem'}),
                search.createColumn({name: 'cost', join: 'memberItem'})
            ]
        });
        var results = getSearchResults(searchObj);

        var items = {};
        for (var i = 0; i < results.length; i++) {
            var result = results[i];
            var parentId = result.getValue('internalid');
            if (typeof items[parentId] === 'undefined') {
                items[parentId] = {
                    item: result.getValue('itemid'),
                    description: result.getValue('salesdescription'),
                    class: result.getText('class'),
                    costcategory: result.getText('costcategory'),
                    finished: result.getValue('custitem3'),
                    components: {}
                };
            }
            var childId = result.getValue('memberitem');
            if (typeof items[parentId]['components'][childId] !== 'undefined') {
                log.debug('WARNING', 'Parent Item: ' + parentId.toString() + ', Repeatd Component Item ' + childId.toString());
            }
            var qty = result.getValue('memberquantity');
            var memberQty = qty ? qty : 0;
            var cost = result.getValue({name: 'cost', join: 'memberItem'});
            var memberCost = cost ? cost : 0;
            items[parentId]['components'][childId] = {
                item: result.getValue({name: 'itemid', join: 'memberItem'}),
                description: result.getValue({name: 'salesdescription', join: 'memberItem'}),
                class: result.getText({name: 'class', join: 'memberItem'}),
                costcategory: result.getText({name: 'costcategory', join: 'memberItem'}),
                source: result.getValue('memberitemsource'),
                quantity: memberQty,
                cost: memberCost
            };
        };
        log.debug('items', JSON.stringify(items));
        return items;
    }

    // Should always look something like:
    // Lot numbered assembly item ---> 
    // Labor Run/Labor Run Overhead (other charge item record)
    // Raw Materials (generally inv item record + raw materials in class)
    function getPlannedStandardCost(assemblyItems) {
        log.debug('getPlannedStandardCost', '>>> START <<<');
        var searchObj = search.create({
            type: 'plannedstandardcost',
            filters: [
                ['location','anyof','10'],
                'AND',
                ['standardcostversion','anyof','26'],
                'AND',
                ['componentitem','noneof','@NONE@']
            ],
            columns: [
                search.createColumn({name: 'internalid', join: 'item', sort: search.Sort.ASC}),
                search.createColumn({name: 'internalid', join: 'componentItem', sort: search.Sort.ASC}),
                search.createColumn({name: 'class', join: 'item'}),
                search.createColumn({name: 'salesdescription', join: 'item'}),
                search.createColumn({name: 'class', join: 'componentItem'}), 
                search.createColumn({name: 'itemid', join: 'componentItem'}),
                search.createColumn({name: 'salesdescription', join: 'componentItem'}),
                search.createColumn({name: 'costcategory', join: 'componentItem'}),
                search.createColumn({name: 'type', join: 'componentItem'}),
                'item',
                'costcategory',
                'componentitem',
                'quantity',
                'cost'
            ]
        });
        var results = getSearchResults(searchObj);
        var items = {};
        for (var i = 0; i < results.length; i++) {
            var result = results[i];
            var parentId = result.getValue({name: 'internalid', join: 'item'});
            if (typeof items[parentId] === 'undefined') {
                items[parentId] = {
                    class: result.getText({name: 'class', join: 'item'}),
                    item: result.getText('item'),
                    description: result.getValue({name: 'salesdescription', join: 'item'}),
                    costcategory: result.getText('costcategory'),
                    components: {}
                };
            }
            var childId = result.getValue({name: 'internalid', join: 'componentItem'});
            var childType = result.getValue({name: 'type', join: 'componentItem'});
            items[parentId]['components'][childId] = {
                item: result.getValue({name: 'itemid', join: 'componentItem'}),
                description: result.getValue({name: 'salesdescription', join: 'componentItem'}),
                class: result.getText({name: 'class', join: 'componentItem'}),
                costcategory: result.getValue({name: 'costcategory', join: 'componentItem'}),
                quantity: result.getValue('quantity'),
                cost: result.getValue('cost'),
                type: childType
            };
            log.debug(parentId + ' to ' + childId);
            // SECOND LEVEL - Get the components that make up the sub-assembly
            if (childType === 'Assembly') {
                log.debug(parentId + ' to ' + childId, 'Getting sub-assembly for ' + childId);
                if (typeof assemblyItems[childId] === 'undefined') {
                    log.debug('WARNING - PSC Componenent', 'Assembly Item record NOT found');
                    continue;
                }
                log.debug('Item Sub-Assembly Found', JSON.stringify(assemblyItems[childId]));
                items[parentId]['components'][childId]['components'] = assemblyItems[childId];
                // There could be a third level.
                // if (assemblyItems[childId]['components'])
            }
        }
        return items;
    }
    
    function getSearchResults(searchObj) {
        var searchResults = searchObj.run(),
            index = 0,
            step = 1000,
            resultSet,
            results = [];
        do {
            resultSet = searchResults.getRange(index, index + step);
            results = results.concat(resultSet);
            index = index + step;
        } while (resultSet.length > 0);
        log.debug('getSearchResults', results);
        log.debug('Search result count', results.length);
        return results;
    }

    exports.onRequest = onRequest;
    return exports;
});