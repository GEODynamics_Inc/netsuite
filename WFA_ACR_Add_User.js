/**
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 */
define(['N/record'], function(record) {

    function newUser(obj) {
        var employee = record.create({type: record.Type.EMPLOYEE, isDynamic: false});
        for (var i in obj) {
            employee.setValue(i, obj[i]);
        }
        employee.setValue('custentity_geo_paycom_id', 'New');
        return employee.save();
    }

    function onAction(context){
        try {
            var cr = context.newRecord;

            if (cr.getValue('custrecord_acr_type') !== '1') {return;}

            var recordId = newUser({
                firstname: cr.getValue('custrecord_acr_first_name'),
                lastname: cr.getValue('custrecord_acr_last_name'),
                supervisor: cr.getValue('custrecord_acr_supervisor'),
                department: cr.getValue('custrecord_acr_department'),
                title: cr.getValue('custrecord_acr_job_title'),
                email: cr.getValue('custrecord_acr_email'),
                subsidiary: cr.getValue('custrecord_acr_subsidiary'),
                location: cr.getValue('custrecord_acr_location'),
                custentity_access_change_request_id: cr.id,
                custentity_access_change_request_log: 'ACR' + cr.id + ' Approved: Role = ' + cr.getText('custrecord_acr_role'),
                custentity_send_new_access_email: true
            });
            log.debug('Access Change Request - New User', recordId);
            record.submitFields({
                type: 'customrecord_access_change_request',
                id: cr.id,
                values: {
                    custrecord_acr_employee: recordId,
                    custrecord_acr_status: 'Approved, Pending Admin Review'
                },
                options: {
                    enableSourcing: false,
                    ignoreMandatoryFields : true
                }
            });
            
        } catch(e) {
            log.error(e.name, e.message);
        }
    }
    return {
        onAction: onAction
    }
});