/**
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 */
define(['N/record'], function(record) {
    function onAction(context){
        try {
            var cr = context.newRecord;
            if (cr.getValue('custrecord_acr_type') !== '2') {return;}
            cr.setValue('custrecord_acr_status', 'Approved');
            
            record.submitFields({
                type: record.Type.EMPLOYEE,
                id: cr.getValue('custrecord_acr_employee'),
                values: {
                    isinactive: true,
                    giveaccess: false,
                    custentity_access_change_request_id: cr.id,
                    custentity_access_change_request_log: 'ACR' + cr.id + ' Approved: Remove User Access'
                },
                options: {
                    enableSourcing: false,
                    ignoreMandatoryFields: true
                }
            });
            
        } catch(e) {
            log.error(e.name, e.message);
        }
    }
    return{
        onAction: onAction
    }
});