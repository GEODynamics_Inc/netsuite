/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 */

define(['N/search', 'N/record', 'N/runtime', 'N/task'], function (search, record, runtime, task) {

    function getRemainingUsage() {
        var scriptObj = runtime.getCurrentScript();
        var units = scriptObj.getRemainingUsage();
        log.debug('', 'Remaining governance units: ' + units);
        return units;
    }

    function getTransactions() {
        var returnObj = {};
        // Load search
        var searchObj = search.load({ id: 2005 });
        // Iterate
        searchObj.run().each(function (result) {
            try {
                var recordId = result.getValue({ name: 'internalid' });
                // Add transaction
                if (typeof returnObj[recordId] === 'undefined') {
                    returnObj[recordId] = [];
                }
                var recordType = result.getValue({ name: 'recordtype' });
                var item = result.getValue({ name: 'item' });
                var lineClass = result.getValue({ name: 'internalid', join: 'class' });
                var lineDepartment = result.getValue({ name: 'internalid', join: 'department' });
                var itemClass = result.getValue({ name: 'class', join: 'item' });
                var itemDepartment = result.getValue({ name: 'department', join: 'item' });
                var documentNumber = result.getValue({ name: 'tranid' });
                var lineObj = {
                    recordType: recordType,
                    item: item,
                    lineClass: lineClass,
                    lineDept: lineDepartment,
                    itemClass: itemClass,
                    itemDept: itemDepartment,
                    documentNumber: documentNumber
                };
                // Append line
                returnObj[recordId].push(lineObj);
                return true;

            } catch (e) {
                log.error(e.name, e.message);
                return true;
            }
        });
        // log.debug('getTransactions', JSON.stringify(returnObj));
        return returnObj;
    }

    function isEmpty(n) {
        if (n === '' || n === null || n === undefined) {
            return true;
        } else {
            if (Array.isArray(n)) {
                if (n.length === 0) {
                    return true;
                }
            }
            if (typeof n === 'string') {
                if (n.trim() === '') {
                    return true;
                }
            }
        }
        return false;
    }

    function reschedule() {
        var scheduledScriptTask = task.create({
            taskType: task.TaskType.SCHEDULED_SCRIPT
        });
        scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
        scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
        return scheduledScriptTask.submit();
    }

    function execute(context) {
        var transactions = getTransactions();
        // Iterate through main line
        for (var i in transactions) {
            try {
                var updated = false;
                var recordType = transactions[i][0].recordType;
                var documentNumber = transactions[i][0].documentNumber;
                var cr = record.load({ type: recordType, id: i });
                // Iterate through lines
                transactions[i].forEach(function (line) {
                    var lineNum = cr.findSublistLineWithValue({ sublistId: 'item', fieldId: 'item', value: line.item });
                    // Skip
                    if (lineNum == -1) { return true; }
                    // Check fields to update
                    if (isEmpty(line.lineClass) === true && isEmpty(line.itemClass) === false) {
                        // Update class
                        log.debug('', 'Updating class on line ' + lineNum + ' to ' + line.itemClass);
                        cr.setSublistValue({ sublistId: 'item', fieldId: 'class', line: lineNum, value: line.itemClass });
                        updated = true;
                    }
                    if (isEmpty(line.lineDept) === true && isEmpty(line.itemDept) === false) {
                        // Update department
                        log.debug('', 'Updating department on line ' + lineNum + ' to ' + line.itemDept);
                        cr.setSublistValue({ sublistId: 'item', fieldId: 'department', line: lineNum, value: line.itemDept });
                        updated = true;
                    }
                    return true;
                });
                if (updated === true) {
                    var recordId = cr.save({ enableSourcing: false, ignoreMandatoryFields: true });
                    log.debug(recordType + ', ' + documentNumber, recordId);
                }
            } catch(e) {
                log.error(e.name, e.message);
                continue;
            }

            // Reschedule if needed
            if (runtime.getCurrentScript().getRemainingUsage() < 100) {
                log.debug('Rescheduling');
                reschedule();
                return;
            }

        }
    }
    return { execute: execute }
});
